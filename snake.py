 
WIDTH = 300
HEIGHT = 300

boxes = [] #håller koll på alla rectanglar skapade 
direction = (0, 0) #vilket håll alla rör sig åt
box_size = (10, 10) # hur stora alla lådor är

background_color = (255, 0, 0) #röd

def generate_rect(x, y):
    """
    Genererar en Rect på position x, y och returnerar det
    """
    rect = Rect(x, y, *box_size)
    return rect

def draw():
    """
    Kallas 60 gånger vajer sekund. Skriver ut varje rektangel med
    angiven färg.
    """
    screen.fill(background_color)
    for box in boxes:
        screen.draw.filled_rect(box, (0, 0, 255)) #blå

def update():
    """
    Flyttar varje låda i rictningen direction
    """
    global boxes, direction
    for box in boxes:
        box.move_ip(direction) # move_ip uppdaterar boxsens position istället
                               # för att skapa en ny som är förflyttad
def on_key_down(key):
    """
    Kallas varje gång en knapp trycks ner. Om det är någon av
    piltangenterna uppdateras förflyttningsriktningen efter det.
    """
    global direction
    what_to_do = {keys.UP: (0, -1),  # 0, 0 ligger uppe till vänster
                  keys.DOWN: (0, 1), # därav minskar y då man förflyttar
                  keys.LEFT: (-1, 0),# sig uppåt och ökar när man går
                  keys.RIGHT: (1, 0)}# neråt.
    try: # detta fångar om man har tryckt ner en annan knap än piltangenterna
        direction = what_to_do[key]
    except KeyError:
        pass

def on_mouse_down(pos):
    """
    Kallas varje gång musen trycks ner. Skapar en rektangel vars nedre
    vänstra hörn beffinners sig vid musens position.
    """
    global boxes, box_size
    boxes.append(generate_rect(pos[0] - box_size[0],pos[1] - box_size[1]))

